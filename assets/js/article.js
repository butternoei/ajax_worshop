$('#form-create-article').submit(function (e) {
   e.preventDefault()
   var data = $(this).serialize()

   $.ajax({
      url: 'controller/article/create_article.php',
      data: data,
      method: 'POST',
      dataType: 'JSON',
      success: function (res) {
         if (res.result == 'success') {
            Swal.fire({
               icon: 'success',
               title: 'เพิ่มข้อมูลเรียบร้อย'
            }).then(function() {
               window.location = 'index.php'
            })
         } else {
            Swal.fire({
               icon: 'error',
               title: 'เกิดข้อผิดพลาดในการเพิ่มข้อมูล',
            })
            console.log(res.msg)
         }
      },
      error: function () {
         Swal.fire({
            icon: 'warning',
            title: 'พบปัญหาในการร้องขอข้อมูล',
         })
      }
   })
})

$('#form-edit-article').submit(function (e) {
   e.preventDefault()
   var data = $(this).serialize()

   $.ajax({
      url: 'controller/article/update_article.php',
      data: data,
      method: 'POST',
      dataType: 'JSON',
      success: function (res) {
         if (res.result == 'success') {
            Swal.fire({
               icon: 'success',
               title: 'แก้ไขข้อมูลเรียบร้อย'
            }).then(function() {
               window.location = 'index.php'
            })
         } else {
            Swal.fire({
               icon: 'error',
               title: 'เกิดข้อผิดพลาดในการแก้ไขข้อมูล',
            })
            console.log(res.msg)
         }
      },
      error: function () {
         Swal.fire({
            icon: 'warning',
            title: 'พบปัญหาในการร้องขอข้อมูล',
         })
      },
   })
})

$('.btn-delete-article').click(function() {
   var a_id = $(this).data('id')

   Swal.fire({
      title: 'โปรดยืนยันเพิ่มลบข้อมูล',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก'
   }).then(function(result) {
      if (result.value) {
         $.ajax({
            url: 'controller/article/delete_article.php',
            data: 'a_id=' + a_id,
            method: 'GET',
            dataType: 'JSON',
            success: function(res) {
               if(res.result == 'success') {
                  Swal.fire({
                     icon: 'success',
                     title: 'ลบข้อมูลเรียบร้อย'
                  }).then(function() {
                     window.location = 'index.php'
                  })
               } else {
                  Swal.fire({
                     icon: 'error',
                     title: 'เกิดข้อผิดพลาดในการลบข้อมูล'
                  })
               }
            },
            error: function() {
               Swal.fire({
                  icon: 'warning',
                  title: 'พบปัญหาในการร้องขอข้อมูล'
               })
            }
         })
      }
   })
})
