<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>

   <?php include_once('common/style/style.inc.php') ?>
</head>

<body>
   <div class="container" style="max-width: 800px">
      <div class="card mt-5">
         <div class="card-body">
            <div class="text-center">
               <h3>เพิ่มบทความ</h3>
            </div>
            <form id="form-create-article">
               <div class="form-group">
                  <label for="a_title">ชื่อเรื่อง</label>
                  <input type="text" name="a_title" id="a_title" class="form-control" required>
               </div>
               <div class="form-group">
                  <label for="a_author">ผู้แต่ง</label>
                  <input type="text" name="a_author" id="a_author" class="form-control" required>
               </div>
               <div class="form-group">
                  <label for="a_status">สถานะ</label>
                  <input type="text" name="a_status" id="a_status" class="form-control" required>
               </div>
               <div class="form-group">
                  <label for="a_weight">น้ำหนัก</label>
                  <input type="text" name="a_weight" id="a_weight" class="form-control" required>
               </div>
               <div class="form-group">
                  <label for="a_start">เริ่ม</label>
                  <input type="date" name="a_start" id="a_start" class="form-control" required>
               </div>
               <!-- <div class="form-group">
                  <label for="a_file">ไฟล์</label>
                  <input type="file" name="a_file" id="a_file" class="form-control" required>
               </div> -->
               <div class="text-right">
                  <button type="submit" class="btn btn-primary">บันทึก</button>
               </div>
            </form>
         </div>
      </div>
   </div>
   
   <?php include_once('common/script/script.inc.php') ?>
   <script src="assets/js/article.js"></script>
</body>

</html>