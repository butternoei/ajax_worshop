<?php
date_default_timezone_set('Asia/Bangkok');

$conn = mysqli_connect('localhost', 'root', '', 'ajax_workshop');
mysqli_set_charset($conn, 'utf8');

if(!$conn) {
   die ('Connect failed');
}

function debug($arr) {
   echo '<pre>';
   print_r($arr);
   echo '</pre>';
}