<?php
require_once('../config.php');

if (isset($_GET['a_id'])) {
   $a_id = $_GET['a_id'];

   $article_sql = "DELETE FROM article WHERE a_id = '$a_id'";
   $article_query = mysqli_query($conn, $article_sql);

   if ($article_query) {
      echo json_encode([
         "result" => "success"
      ]);
   } else {
      echo json_encode([
         "reult" => "failed",
         "msg" => "Update article error: " . mysqli_error($conn)
      ]);
   }
} else {
   header('../../index.php');
}

mysqli_close($conn);