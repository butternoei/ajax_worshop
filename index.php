<?php
require_once('controller/config.php');
require_once('common/script/pagination.inc.php');

// -- Page pagination --
$num = 0;
$end_page = 12;
$step_num = 0;
$curent_page = isset($_GET['page']) ? $_GET['page'] : 1;

if (!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == 1)) {
   $_GET['page'] = 1;
   $step_num = 0;
   $start_page = 0;
} else {
   $start_page = $_GET['page'] - 1;
   $step_num = $_GET['page'] - 1;
   $start_page = $start_page * $end_page;
}
// -- Page pagination --

// -- Count row in table --
$article_sql = "SELECT * FROM article";
$article_query = mysqli_query($conn, $article_sql);
$count_row = mysqli_num_rows($article_query);
// -- Count row in table --


$article_sql = "SELECT * FROM article ORDER BY a_id ASC LIMIT $start_page, $end_page";
$article_query = mysqli_query($conn, $article_sql);
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   
   <?php include_once('common/style/style.inc.php') ?>
</head>

<body>
   <div class="container">
      <div class="text-right mt-5">
         <a href="form_create.php">เพิ่มข้อมูล</a>
      </div>
      <table class="table table-bordered table-striped table-hover">
         <thead>
            <th>ชื่อเรื่อง</th>
            <th>ผู้แต่ง</th>
            <th>สถานะ</th>
            <th>น้ำหนัก</th>
            <th>เริ่ม</th>
            <th>ไฟล์</th>
            <th class="text-center">ตัวเลือก</th>
         </thead>
         <tbody>
            <?php while ($row = mysqli_fetch_assoc($article_query)) { ?>
               <tr>
                  <td><?= $row['a_title'] ?></td>
                  <td><?= $row['a_author'] ?></td>
                  <td><?= $row['a_status'] ?></td>
                  <td><?= $row['a_weight'] ?></td>
                  <td><?= $row['a_start'] ?></td>
                  <td><?= $row['a_file'] ?></td>
                  <td class="text-center">
                     <a href="form_edit.php?a_id=<?= $row['a_id'] ?>"><i class="far fa-edit"></i></a>
                     <span class="text-danger btn-delete-article" data-id="<?= $row['a_id'] ?>">
                        <i class="far fa-trash-alt"></i>
                     </span>
                  </td>
               </tr>
            <?php } ?>
         </tbody>
      </table>

      <div class="text-center">
         <?php page_navi($count_row, $curent_page, $end_page); ?>
      </div>
   </div>

   <?php include_once('common/script/script.inc.php') ?>
   <script src="assets/js/article.js"></script>
</body>

</html>