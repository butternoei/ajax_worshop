<?php
require_once('controller/config.php');

if (isset($_GET['a_id'])) {
   $a_id = $_GET['a_id'];
   $article_sql = "SELECT * FROM article WHERE a_id = '$a_id'";
   $article_query = mysqli_query($conn, $article_sql);
   $article = mysqli_fetch_assoc($article_query);
} else {
   header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>

   <?php include_once('common/style/style.inc.php') ?>
</head>

<body>
   <div class="container" style="max-width: 800px">
      <div class="card mt-5">
         <div class="card-body">
            <div class="text-center">
               <h3>แก้ไขบทความ</h3>
            </div>
            <div class="text-right">
               <a href="index.php" class="btn btn-primary">
                  <i class="fas fa-arrow-left"></i>
                  ย้อนกลับ
               </a>
            </div>
            <form id="form-edit-article">
               <input type="hidden" name="a_id" value="<?= $article['a_id'] ?>" required>
               <div class="form-group">
                  <label for="a_title">ชื่อเรื่อง</label>
                  <input type="text" name="a_title" id="a_title" class="form-control" value="<?= $article['a_title'] ?>" required>
               </div>
               <div class="form-group">
                  <label for="a_author">ผู้แต่ง</label>
                  <input type="text" name="a_author" id="a_author" class="form-control" value="<?= $article['a_author'] ?>" required>
               </div>
               <div class="form-group">
                  <label for="a_status">สถานะ</label>
                  <input type="text" name="a_status" id="a_status" class="form-control" value="<?= $article['a_status'] ?>" required>
               </div>
               <div class="form-group">
                  <label for="a_weight">น้ำหนัก</label>
                  <input type="text" name="a_weight" id="a_weight" class="form-control" value="<?= $article['a_weight'] ?>" required>
               </div>
               <div class="form-group">
                  <label for="a_start">เริ่ม</label>
                  <input type="date" name="a_start" id="a_start" class="form-control" value="<?= $article['a_start'] ?>" required>
               </div>
               <!-- <div class="form-group">
                  <label for="a_file">ไฟล์</label>
                  <input type="file" name="a_file" id="a_file" class="form-control" required>
               </div> -->
               <div class="text-right">
                  <button type="submit" class="btn btn-primary">บันทึก</button>
               </div>
            </form>
         </div>
      </div>
   </div>

   <?php include_once('common/script/script.inc.php') ?>
   <script src="assets/js/article.js"></script>
</body>

</html>