-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2020 at 08:36 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajax_workshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `a_id` int(5) NOT NULL,
  `a_title` varchar(150) NOT NULL,
  `a_author` varchar(100) NOT NULL,
  `a_status` varchar(10) NOT NULL,
  `a_weight` varchar(5) NOT NULL,
  `a_start` date NOT NULL,
  `a_file` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`a_id`, `a_title`, `a_author`, `a_status`, `a_weight`, `a_start`, `a_file`) VALUES
(1, 'wowo', 'wowo', 'wowo', 'wowo', '2020-07-11', ''),
(2, 'asdfa', 'sdfasdf', 'fsdf', 'sdfsd', '2020-07-11', ''),
(3, 'asdfa', 'sdfasdf', 'fsdf', 'sdfsd', '2020-07-11', ''),
(5, 'Butter is fat ', 'noei', 'student', '-', '2020-07-11', ''),
(6, 'wowo', 'wowo', 'wowo', 'wowo', '2020-07-11', ''),
(7, 'Butter is fat ', 'wowo', 'fsdf', 'wowo', '2020-07-11', ''),
(8, 'Butter is fat ', 'wowo', 'fsdf', 'wowo', '2020-07-11', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`a_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `a_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
